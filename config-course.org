# -*- eval: (visual-line-mode) -*-

# SPDX-FileCopyrightText: 2019-2021 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

#+DATE: Computer Structures and Operating Systems 2021
#+AUTHOR: Jens Lechtenbörger
#+REVEAL_ACADEMIC_TITLE: Dr.

#+INCLUDE: "config.org"

# Starting with 2019, use generic link instead of quizzes.
# Can still be overwritten per course.
#+MACRO: jittquiz {{{learnweb}}}
